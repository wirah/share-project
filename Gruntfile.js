module.exports = function (grunt) {

	var environmentSetup = 'env:'+( process.env.NODE_ENV || 'development' );

    var path = require('path');

    var mongoConsoleOptions = {
        stdout:false,
        stderr:true,
        failOnError:true
    };

    var defaultConsoleOptions = {
        stdout:true,
        stderr:true,
        failOnError:true
    };

    grunt.initConfig({
        env: {
        		development: {
        			MONGO_DB: 'iceberg_development',
        			MONGO_URL: 'localhost'
        		}
        },
        pkg: grunt.file.readJSON('package.json'),
        express:{
            livereload:{
                options:{
                    port:9000,
                    bases:path.resolve('angular-app/app'),
                    monitor:{},
                    debug:true,
                    server:path.resolve('express-app/app.js')
                }
            }
        },
        karma: {
            unit: { configFile:'angular-app/config/karma.conf.js', singleRun: true },
            e2e: { configFile:'angular-app/config/karma-e2e.conf.js', singleRun:true },
            watch: { configFile:'angular-app/config/karma.conf.js', singleRun:false, autoWatch:true },
            watch_e2e: { configFile:'angular-app/config/karma-e2e.conf.js', singleRun:false, autoWatch:true }
        },
        simplemocha:{
            options:{
                globals:['should'],
                timeout:3000,
                ignoreLeaks:false,
                ui:'bdd',
                reporter: "<%= process.env.NODE_ENV == 'functional' ? 'tap' : 'spec' %>"
            },
            all:{ src:['express-app/test/**/*.spec.js'] }
        },
        watch:{
            scripts:{
                files:['express-app/test/**/*.spec.js', 'express-app/models/*.js'],
                tasks:['simplemocha']
            }
        },
		exec: {
            "install-global-dependencies": {
                command: 'npm install -g forever'
            },
			"update-dependencies": {
			  		command: 'npm install'
			}
		}
    });

    grunt.loadNpmTasks('grunt-simple-mocha');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-contrib-livereload');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-env');

    grunt.registerTask('test-backend', [environmentSetup, 'exec:update-dependencies', 'simplemocha']);

    grunt.registerTask('test-frontend', [environmentSetup, 'exec:update-dependencies', 'express', 'karma:unit', 'karma:e2e']);
    grunt.registerTask('watch-frontend', [environmentSetup, 'exec:update-dependencies', 'express', 'karma:watch_e2e']);

    grunt.registerTask('demo-app', [environmentSetup, 'exec:update-dependencies', 'livereload-start', 'express', 'watch']);
    grunt.registerTask('run-app', [environmentSetup, 'exec:update-dependencies', 'livereload-start', 'express', 'watch']);
    grunt.registerTask('cluster-app', [environmentSetup, 'exec:update-dependencies', 'exec:cluster']);

    grunt.registerTask('package',['exec:package-app']);

    grunt.registerTask('release',['exec:update-dependencies', 'exec:install-global-dependencies', 'exec:rpm-build', 'exec:rpm-publish']);

    grunt.registerTask('default', ['run-app']);
};
